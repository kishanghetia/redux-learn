import React, { useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { loadMovies, updateSearchBox } from "../redux/movieActios";

function Movies() {
  const moviDumiData = [
    {
      name: "KGF",
      realseDate: "10-12-2022",
    },
    {
      name: "Batman",
      realseDate: "23-09-2020",
    },
  ];

  const movies = useSelector((state) => state.movieReducer);
  const dispatch = useDispatch();
  console.log("movie reducer >>>>>", movies);

  useEffect(() => {
    dispatch(loadMovies(moviDumiData));
  }, []);

  return (
    <div>
      Movies
      <button
        onClick={() => {
          dispatch(updateSearchBox("kgf"));
        }}
      >
        click hear
      </button>
    </div>
  );
}

export default Movies;
