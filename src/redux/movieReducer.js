import { LOAD_MOVIES, UPDATE_SEARCH_BOX } from "./types";

const initialState = {
  searchBoxText: "",
  movieData: undefined,
};

const movieReducer = (state = initialState, action) => {
  console.log("action is here ---", action.payload, "---");
  switch (action.type) {
    case LOAD_MOVIES:
      return {
        ...state,
        movieData: action.payload,
      };
    case UPDATE_SEARCH_BOX:
      return {
        ...state,
        searchBoxText: action.payload,
      };

    default:
      return state;
  }
};

export default movieReducer;
